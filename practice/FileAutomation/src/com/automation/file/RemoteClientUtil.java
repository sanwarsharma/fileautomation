package com.automation.file;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class RemoteClientUtil 
{

	protected static boolean isNewFileExistWithExtension(String[] fileExtensions)
	{
		Set<String> extensionSet = new HashSet<String>();
		extensionSet.addAll(Arrays.asList(fileExtensions));
		File f = new File("E://File");

		File[] files = f.listFiles();

		for (int indx = 0; indx < files.length; indx++) 
		{
			if(extensionSet.contains(getFileIndex(files[indx].getName())))
			{
				return true;
			}
		}
		return false;
	}

	protected static List<String> getNewFilesFromDir(String[] fileExtensions)
	{
		Set<String> extensionSet = new HashSet<String>();
		extensionSet.addAll(Arrays.asList(fileExtensions));
		File f = new File("E://File");

		File[] files = f.listFiles();

		List<String> fileNamesList = new ArrayList<String>();
		for (int indx = 0; indx < files.length; indx++) 
		{
			if(extensionSet.contains(getFileIndex(files[indx].getName())))
			{
				fileNamesList.add(files[indx].getName());
			}
		}

		return fileNamesList;
	}

	private static String getFileIndex(String fileName)
	{
		String extension = "";

		int i = fileName.lastIndexOf('.');
		if (i >= 0)
		{
		    extension = fileName.substring(i+1);
		}
		return extension;
	}

}
