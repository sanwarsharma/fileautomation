package com.automation.file;

import java.util.List;

public class Client
{
	public static void main(String[] args)
	{
		doAutomationForFiles(args);
	}

	public static void doAutomationForFiles(String[] fileExtensions)
	{
		if(!RemoteClientUtil.isNewFileExistWithExtension(fileExtensions))
		{
			return;
		}
		List<String> reomteFiles = RemoteClientUtil.getNewFilesFromDir(fileExtensions);
		if(reomteFiles.isEmpty())
		{
			return;
		}
		for (String file : reomteFiles)
		{
			System.out.println(file);
		}
	}
}
